# idleMaster (XFCE and KDE Plasma's versions)
  Fixes annoying screensaver activation during videos, hibernation when torrenting or listening to music etc.

This script constantly takes screenshots and compares them to define if there was a movement on the screen to prevent screensaver from ruining your movie time.

When using sertain whitelisted apps, it will prevent hibernation (actually, postpone, until whitelisted apps are closed).

Things to know:
  * Configuration is needed only for whitelisted apps (they will keep PC running with screen off - define them in WHITELIST array).
  * This script is optimised for any desktop environment.
  * You need to have bc, imagemagick, pulseaudio, xdotool, xprintidle, xssstate.
  * You need to have either 'scrot' (XFCE) or 'maim' (KDE Plasma).
  * You can adjust sensivity of movement detection - define it in PRECISION variable.
  * You can adjust screenshot delay - define it in DELAY variable.

Variables:

* WHITELIST=("transmission-gtk" ) # Programmes that prevent hibernation (screen can turn off)
* PRECISION=91.5 # Minimum tolerable integrity (rate) - value too high will make clock prevent screensaver
* DELAY=1 # Screensaver delay or less (frequency of screenshots to detect screen redraws)

There are several things to note about how script works (they are not bugs):

* Your PC will not sleep while the deskop with slideshow. Set slideshow delay to be more than your screensaver delay.
* Your PC will not sleep when the browser tab with blinking ads/animations/objects moving constantly is open. Scroll away from the shiny part or use adBlock.
* Your PC will not hibernate if you leave whitelisted app running. Remove app from whitelist / close app / ensure app automatic shutdown.
* Your PC will not hibernate if some audio is playing (advertisement with sound, for example).

You are free to use the code as you wish.

TO DO:
* I need to find the right way to track file transfers and other similar processes (maybe to script whitelisting app in question, doing stuff, dewhitelisting?)
